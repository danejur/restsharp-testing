﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestTesting.Common
{
    public class SubValueClass
    {
        public string SubValue { get; set; }
        public Guid SubValueId { get; set; }
    }
}
