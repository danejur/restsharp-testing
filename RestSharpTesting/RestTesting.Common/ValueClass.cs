﻿using System;
using System.Collections.Generic;

namespace RestTesting.Common
{
    public class ValueClass
    {
        public string Value { get; set; }
        public Guid ValueId { get; set; }

        public List<SubValueClass> SubValues { get; set; }
    }
}
