﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestSharpTesting
{
    public class FloorPlan
    {
        public string Name { get; set; }
        public string ExternalId { get; set; }
        public int SquareFeet { get; set; }
        public Guid PropertyId { get; set; }
    }
}
