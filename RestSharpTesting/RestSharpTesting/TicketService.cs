﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RestSharp;
using RestSharp.Authenticators;

namespace RestSharpTesting
{
    public class TicketService
    {
        private static Guid TenantId = new Guid("d8cb3898-3250-4ea1-a308-ef66d2a18c2c");
        private static string _baseUrl = "https://integrationringzero.azurewebsites.net/api/v1/";

        public TicketService()
        {}

        public static async Task<List<Ticket>> GetTickets()
        {
            var client = new RestClient(_baseUrl);
            var request = new RestRequest($"{TenantId}/Ticket", Method.GET);
            var response = await client.ExecuteTaskAsync<List<Ticket>>(request);

            return response.Data;
        }

        public static async Task<List<Ticket>> GetPastDueTickets()
        {
            List<Ticket> tickets = await GetTickets();
            return tickets.Where((x) => x.DateDue > DateTime.Now).ToList();
        }

        public static List<Ticket> GetPastDueTickets(List<Ticket> tickets)
        {
            return tickets.Where((x) => x.DateDue > DateTime.Now).ToList();
        }

        public static async Task<List<Ticket>> GetCompletedTickets()
        {
            List<Ticket> tickets = await GetTickets();
            return tickets.Where((x) => x.DateCompleted != null).ToList();
        }

        public static List<Ticket> GetCompletedTickets(List<Ticket> tickets)
        {
            return tickets.Where((x) => x.DateCompleted != null).ToList();
        }

        public static async Task<Ticket> GetTicket(Guid id)
        {
            var client = new RestClient(_baseUrl);
            var request = new RestRequest($"{TenantId}/Ticket/{id}", Method.GET);
            var response = await client.ExecuteTaskAsync<Ticket>(request);

            return response.Data;
        }

        public static async Task<Ticket> PostTicket(Ticket ticket)
        {
            var client = new RestClient(_baseUrl);
            var request = new RestRequest($"{TenantId}/Ticket", Method.POST);
            request.AddJsonBody(ticket);
            var response = await client.ExecuteTaskAsync<Ticket>(request);

            return response.Data;
        }

        public static async Task<Ticket> UpdateTicket(Guid id, Ticket updatedTicket)
        {
            var client = new RestClient(_baseUrl);
            var request = new RestRequest($"{TenantId}/Ticket/{id}", Method.PUT);
            request.AddJsonBody(updatedTicket);
            var response = await client.ExecuteTaskAsync<Ticket>(request);

            return response.Data;
        }

        public static async Task<Ticket> DeleteTicket(Guid id)
        {
            var client = new RestClient(_baseUrl);
            var request = new RestRequest($"{TenantId}/Ticket/{id}", Method.DELETE);
            var response = await client.ExecuteTaskAsync<Ticket>(request);

            return response.Data;
        }
    }
}
