﻿using System;
namespace RestSharpTesting
{
    public enum PersistState
    {
        None = 0,

        /// <summary>
        /// Used to denote that an entity is active in its persistence store
        /// </summary>
        Active,

        /// <summary>
        /// Used to denote that an entity is inactive in its persistence store
        /// </summary>
        Inactive,

        /// <summary>
        /// Used to Identify a Entity is marked for Deletion
        /// </summary>
        MarkedForDeletion,

        /// <summary>
        /// Used to Identify a Entity is Marked for Archiving
        /// </summary>
        MarkedForArchiving,

        /// <summary>
        /// Used to Identify a Entity is Marked for Purging
        /// </summary>
        MarkedForPurging
    }
}
