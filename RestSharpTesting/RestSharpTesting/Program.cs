﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using RestSharp;

namespace RestSharpTesting
{
    class Program
    {
        static void Main(string[] args)
        {
            var user = new Guid();

            var ticket = new Ticket
            {
                id = new Guid(),
                tenantId = new Guid("d8cb3898-3250-4ea1-a308-ef66d2a18c2c"),
                created = DateTimeOffset.Now,
                modified = DateTimeOffset.Now,
                createdBy = user,
                lastModifiedBy = user,
                persistStatus = PersistState.None,
                Images = new List<Image>(),
                Details = "This is a test ticket"
            };
            var postedTicket = TicketService.PostTicket(ticket).Result;
            var tickets = TicketService.GetTickets().Result;
            var ticketById = TicketService.GetTicket(postedTicket.id).Result;
            Console.WriteLine(tickets);
        }

        private static async Task DoThings()
        {
            var client = new RestClient("<your_endpoint_here>");

            var request = new RestRequest("{tenantId}/FloorPlan", Method.GET);

            request.AddUrlSegment("tenantId", "d8cb3898-3250-4ea1-a308-ef66d2a18c2c");

            var regularResponse = await client.ExecuteTaskAsync(request);

            var hydratedResponse = await client.ExecuteTaskAsync<IEnumerable<FloorPlan>>(request);
        }
    }
}
