﻿using System;
namespace RestSharpTesting
{
    public class Image
    {
        public Guid Id { get; set; }
        public string Uri { get; set; }
        public string OriginalName { get; set; }
        public string Name { get; set; }
    }
}
