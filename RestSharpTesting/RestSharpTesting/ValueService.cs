﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using RestTesting.Common;

namespace RestSharpTesting
{
    public class ValueService
    {
        private const string BaseUri = "https://rest-testing.azurewebsites.net/api/";

        public async Task<List<ValueClass>> GetValues()
        {
            var restClient = new RestClient(BaseUri);

            var req = new RestRequest("Values", Method.GET);

            var resp = await restClient.ExecuteTaskAsync<List<ValueClass>>(req);

            return resp.Data;
        }
    }
}
