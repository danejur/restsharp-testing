﻿using System;
using System.Collections.Generic;

namespace RestSharpTesting
{
    public class Ticket
    {
        public string Details { get; set; }

        public DateTimeOffset? DateDue { get; set; }
        public DateTimeOffset? DateStarted { get; set; }
        public DateTimeOffset? DateCompleted { get; set; }

        public List<Image> Images { get; set; }
        public Guid id;
        public Guid tenantId;
        public DateTimeOffset created;
        public DateTimeOffset modified;
        public Guid createdBy;
        public Guid lastModifiedBy;
        public PersistState persistStatus;
    }
}
