﻿using System;
using System.Threading.Tasks;
using RestSharpTesting;

namespace RestTesting.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            DoStuffAsync().Wait();
        }

        private static async Task DoStuffAsync()
        {
            System.Console.WriteLine("Getting Vals...");

            var service = new ValueService();

            var vals = await service.GetValues();

            vals.ForEach(x => System.Console.WriteLine(x.Value));
        }
    }
}
