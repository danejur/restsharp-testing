﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RestSharpTesting;
using RestTesting.Common;

namespace RestTesting.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        public static List<ValueClass> Values { get; set; } = new List<ValueClass>();

        // GET api/values
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(Values);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            return Ok(Values[id]);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post(string value)
        {
            Values.Add(new ValueClass { Value = value, ValueId = Guid.NewGuid() });
            return Ok(Values.Count - 1);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, string value)
        {
            var val = Values[id];

            val.Value = value;

            return Ok(val);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Values.RemoveAt(id);
        }
    }
}
